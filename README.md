# GnuPG 2.4.x for Debian

This repository provides GitLab CI/CD tooling to build GnuPG version
2.2.40 and version 2.4.0 for Debian bullseye amd64.

I suggest to report any problems with the packages upstream here:
https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1022702

To install version 2.2.40 follow this:

```
podman run -it --rm debian:bullseye
apt-get update
apt-get dist-upgrade -u -y
apt-get install -y ca-certificates
echo "deb [trusted=yes] https://gitlab.com/debdistutils/packages/libgpg-error/-/jobs/4092717327/artifacts/raw/aptly bullseye main" > /etc/apt/sources.list.d/libgpg-error.list
echo "deb [trusted=yes] https://gitlab.com/debdistutils/packages/gnupg2/-/jobs/4092793555/artifacts/raw/aptly bullseye main" > /etc/apt/sources.list.d/gnupg2.list
apt-get update
apt-get install -y gnupg2
gpg --version
```

The expected output after all the installation noise should be:

```
gpg (GnuPG) 2.2.40
libgcrypt 1.8.8
Copyright (C) 2022 g10 Code GmbH
License GNU GPL-3.0-or-later <https://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Home: /root/.gnupg
Supported algorithms:
Pubkey: RSA, ELG, DSA, ECDH, ECDSA, EDDSA
Cipher: IDEA, 3DES, CAST5, BLOWFISH, AES, AES192, AES256, TWOFISH,
        CAMELLIA128, CAMELLIA192, CAMELLIA256
Hash: SHA1, RIPEMD160, SHA256, SHA384, SHA512, SHA224
Compression: Uncompressed, ZIP, ZLIB, BZIP2
```

To install version 2.4.0 follow this:

```
podman run -it --rm debian:bullseye
apt-get update
apt-get dist-upgrade -u -y
apt-get install -y ca-certificates
echo "deb [trusted=yes] https://gitlab.com/debdistutils/packages/libgpg-error/-/jobs/4092717327/artifacts/raw/aptly bullseye main" > /etc/apt/sources.list.d/libgpg-error.list
echo "deb [trusted=yes] https://gitlab.com/debdistutils/packages/libgcrypt/-/jobs/4093099318/artifacts/raw/aptly bullseye main" > /etc/apt/sources.list.d/libgcrypt.list
echo "deb [trusted=yes] https://gitlab.com/debdistutils/packages/libksba/-/jobs/4092985161/artifacts/raw/aptly bullseye main" > /etc/apt/sources.list.d/libksba.list
echo "deb [trusted=yes] https://gitlab.com/debdistutils/packages/gnupg2/-/jobs/4093118295/artifacts/raw/aptly bullseye main" > /etc/apt/sources.list.d/gnupg2.list
apt-get update
apt-get install -y gnupg2
gpg --version
```

The expected output after all the installation noise should be:

```
gpg (GnuPG) 2.4.0
libgcrypt 1.10.1
Copyright (C) 2021 Free Software Foundation, Inc.
License GNU GPL-3.0-or-later <https://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Home: /root/.gnupg
Supported algorithms:
Pubkey: RSA, ELG, DSA, ECDH, ECDSA, EDDSA
Cipher: IDEA, 3DES, CAST5, BLOWFISH, AES, AES192, AES256, TWOFISH,
        CAMELLIA128, CAMELLIA192, CAMELLIA256
Hash: SHA1, RIPEMD160, SHA256, SHA384, SHA512, SHA224
Compression: Uncompressed, ZIP, ZLIB, BZIP2
```
